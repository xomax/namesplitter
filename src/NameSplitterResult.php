<?php declare(strict_types=1);

namespace xs\Support;

class NameSplitterResult
{

	/** @var array<string> */
	private $firstNames = [];

	/** @var array<string> */
	private $lastNames = [];

	/**
	 * @param array<string> $firstNames
	 * @param array<string> $lastNames
	 */
	public function __construct(array $firstNames = [], array $lastNames = [])
	{
		$this->firstNames = $firstNames;
		$this->lastNames = $lastNames;
	}

	/**
	 * @return array<string>
	 */
	public function getFirstNames(): array
	{
		return $this->firstNames;
	}

	/**
	 * @return array<string>
	 */
	public function getLastNames(): array
	{
		return $this->lastNames;
	}

	public function getFirstName(): string
	{
		return implode(' ', $this->firstNames);
	}

	public function getLastName(): string
	{
		return implode(' ', $this->lastNames);
	}

}
