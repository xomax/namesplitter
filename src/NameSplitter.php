<?php declare(strict_types=1);

namespace xs\Support;

/**
 * Tool for splitting full name into surname and first name(s)
 * Copyright (c) 2015 Tom Hnatovsky (http://hnatovsky.cz)
 */

class NameSplitter
{

	/** @var array<string>|null */
	private $data = null;

	/** @var string */
	private $dataFile = __DIR__ . '/../data.php';

	public function __construct()
	{
		$this->loadData();
	}

	public function splitName(string $fullName): NameSplitterResult
	{
		$firstNames = [];
		$lastNames = [];

		$fullName = trim($fullName);

		if ($fullName === '') {
			return new NameSplitterResult();
		}

		$parts = explode(' ', $fullName);
		$partCount = count($parts);

		foreach ($parts as $part) {
			if ($this->isFirstName($part)) {
				$firstNames[] = $part;
			} else {
				$lastNames[] = $part;
			}
		}

		if (count($lastNames) === 0 && $partCount > 1) {
			$lastNames[] = (string) array_pop($firstNames);
		}

		return new NameSplitterResult($firstNames, $lastNames);
	}

	private function isFirstName(string $name): bool
	{
		return is_array($this->data) && count($this->data) > 0 && in_array(
			mb_strtoupper($name, 'utf-8'),
			$this->data,
			true
		);
	}

	private function loadData(): void
	{
		if (!file_exists($this->dataFile)) {
			return;
		}

		$this->data = include $this->dataFile;
	}

}
