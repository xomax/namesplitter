Name Splitter library
=====================

Name Splitter is a small library to split full name into surname and first name(s). 

Database
--------

Library uses a first names database provided by Czech Ministry of the Interior. 
You can easily modify or completely replace the database file.

Current source: http://www.mvcr.cz/clanek/cetnost-jmen-a-prijmeni-722752.aspx

Database file contains a simple PHP array returned after inclusion.


How to use
----------

```php
$splitter = new \xs\Support\NameSplitter;
var_dump($splitter->splitName('Ta Duc Trunc'));
// prints object(xs\Support\NameSplitterResult)#2 (2) {
//  ["firstNames":"xs\Support\NameSplitterResult":private]=>
//  array(2) {
//    [0]=>
//    string(2) "Ta"
//    [1]=>
//    string(3) "Duc"
//  }
//  ["lastNames":"xs\Support\NameSplitterResult":private]=>
//  array(1) {
//    [0]=>
//    string(5) "Trunc"
//  }
//}
```